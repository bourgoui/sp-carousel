import { Version } from '@microsoft/sp-core-library';
import {
  BaseClientSideWebPart,
  IPropertyPaneConfiguration,
  IPropertyPaneDropdownOption,
  PropertyPaneTextField,
  PropertyPaneDropdown,
  PropertyPaneSlider
} from '@microsoft/sp-webpart-base';
import { SPComponentLoader } from '@microsoft/sp-loader';
import { SPHttpClient, SPHttpClientResponse, ISPHttpClientOptions } from '@microsoft/sp-http';
import { sp } from "@pnp/sp";
import "@pnp/sp/webs";
import "@pnp/sp/lists";
import "@pnp/sp/items";
import { escape } from '@microsoft/sp-lodash-subset';

import styles from './CarouselWebPart.module.scss';
import * as strings from 'carouselWebPartStrings';
import { ICarouselWebPartProps } from './ICarouselWebPartProps';

import * as jQuery from 'jquery';
import 'bootstrap';


export interface ICarouselItems{
  value: ICarouselItem[];
}

export interface ICarouselItem {
  File: {ServerRelativeUrl: string;};
  Title: string;
  Description: string;
  Link: string;
}

export default class CarouselWebPart extends BaseClientSideWebPart<ICarouselWebPartProps> {
  private _carouselOptions: IPropertyPaneDropdownOption[] = [];

  // build Carousel options
  private _buildCarouselOptions(): void {
    this._getPicLibraries()
      .then((response) => {
        //console.log(response);
        if (response) {
          this._carouselOptions = response.map((list) => {
            return {
              key: list.Id,
              text: list.Title
            };   
          });
        }
      });
   
  }

  // Get all picture libraries
  private _getPicLibraries(): Promise<any> {
    const spOpts: ISPHttpClientOptions = {
      headers: {
        "Accept": "application/json;odata=verbose",
        "Content-Type": "application/json;odata=verbose",
      }
    };
    // Get all Pic
    return sp.web.lists.select(`Id,Title`).filter(`BaseTemplate eq 109`).get().then((list: any) => {
      return list;
    });
    
  }

  // Get the selected list items
  private _getCarouselItems():Promise<ICarouselItems> {
    const spOpts: ISPHttpClientOptions = {
      headers: {
        "Accept": "application/json;odata=verbose",
        "Content-Type": "application/json;odata=verbose",
      }
    };
    return sp.web.lists.getById(this.properties.libName).items.expand(`File`).select(`Title,Description,Link,File/ServerRelativeUrl`)
    .filter(`Enabled eq 1`).orderBy(`ReverseOrder`).getAll().then((items: ICarouselItem[]) => {
      //console.log(items);
      return {value: items};
    });
  }

  private _itemCarouselIndicators(index: number): string {
    if (index == 0){
      return `<li data-target="#featuredCarousel" data-slide-to="${index}" class="active"></li>`;
    }
    else {
      return `<li data-target="#featuredCarousel" data-slide-to="${index}"></li>`;
    }
  }

  private _itemCarouselWrapper(item: ICarouselItem, index: number): string {
    
      return `
      <div class="carousel-item ${index ? ``: `active`}">
        <div class="card border-0 rounded-0 text-light overflow zoom">
          <div class="position-relative">
            <!--thumbnail img-->
            <div class="ratio_left-cover-1 image-wrapper">
                <a href="${item.Link}">
                    <img class="img-fluid w-100"
                        src="${item.File.ServerRelativeUrl}"
                        alt="${item.Title}">
                </a>
            </div>
            <div class="position-absolute p-2 p-lg-3 b-0 w-100 bg-shadow">
                <!--title-->
                <a href="${item.Link}">
                    <h3 class="h4 post-title text-white my-1">${item.Title}</h3>
                </a>
                <!-- meta description -->
                <div class="news-meta">
                    <span class="news-description">${item.Description ? item.Description : ``}</span>
                </div>
            </div>
          </div>
        </div>
      </div>`;
   
  }

  // Initialize Context for PnP JS - https://pnp.github.io/pnpjs/getting-started/#establish-context
  protected onInit(): Promise<void> {

    return super.onInit().then(_ => {
  
      // other init code may be present
  
      sp.setup({
        spfxContext: this.context
      });
    });
  }

  public render(): void {
    SPComponentLoader.loadCss("https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css");
    SPComponentLoader.loadScript("https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js");
    // Populate Properties with Pic Libraries
    if (this.renderedOnce === false) this._buildCarouselOptions();
    // Show Carousel
    if (this.properties.libName) {
      this._getCarouselItems()      
      .then((response: ICarouselItems): void => {
        this.domElement.innerHTML = `
          <div id="carouselContainer" class="${styles.carouselContainer}">
            <div id="featuredCarousel" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators top-indicator"></ol>
              <!-- Wrapper For Slides -->
              <div class="carousel-inner"></div>
              <a class="carousel-control-prev" href="#featuredCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#featuredCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
          </div>`;
        // add carousel items to domElement
        if (response.value.length > 0){
          response.value.forEach((item: ICarouselItem, index: number): void =>{
            //this.domElement.querySelector('.carousel-indicators').innerHTML += this._itemCarouselIndicators(index);
            //this.domElement.querySelector('.carousel-inner').innerHTML += this._itemSlideWrapper(item, index);
            jQuery('.carousel-indicators', this.domElement).append(this._itemCarouselIndicators(index));
            jQuery('.carousel-inner', this.domElement).append(this._itemCarouselWrapper(item, index));
          });
        }

        // initialize the carousel (default to 4000 ms)
        jQuery('#featuredCarousel').carousel({ interval: this.properties.sliderSpeed });

      });
    } else {
      this.domElement.innerHTML = `
          <div id="carouselContainer" class="${styles.carouselContainer}">
            <h4> Please select a picture library in the configuration panel. <h4>
          </div>`;
    }
      
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('title', {
                  label: strings.TitleFieldLabel
                }),
                PropertyPaneDropdown("libName", {
                  label: strings.ListNameFieldLabel,
                  options: this._carouselOptions,
                  selectedKey: 0,
                }),
                PropertyPaneSlider("sliderSpeed", {  
                  label: strings.CarouselSpeedLabel,  
                  min:2000,  
                  max:7000,  
                  value:4000,  
                  showValue:true,  
                  step:1000
                })  
              ]
            }
          ]
        }
      ]
    };
  }
}