export interface ICarouselWebPartProps {
  title: string;
  libName: string;
  sliderSpeed: number;
  }