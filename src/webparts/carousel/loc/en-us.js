define([], function() {
  return {
    "PropertyPaneDescription": "Configuration",
    "BasicGroupName": "Settings",
    "DescriptionFieldLabel": "Description Field",
    "TitleFieldLabel": "Title",
    "ListNameFieldLabel": "Carousel Library",
    "CarouselSpeedLabel": "Carousel Speed (ms)"
  }
});