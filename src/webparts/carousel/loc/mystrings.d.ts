declare interface ICarouselWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  TitleFieldLabel: string;
  ListNameFieldLabel: string;
  CarouselSpeedLabel: string;
}

declare module 'carouselWebPartStrings' {
  const strings: ICarouselWebPartStrings;
  export = strings;
}
